#!/bin/python3
import random
import time

def main():
    
    continuer = True
    while continuer:
        afficher_resultat()
        choix = input("veux tu continuer à reviser tes tables? o/n  ")
        if choix.lower() == "n":
            continuer = False

def demander_table():
    '''demander le choix d'une table
    param : table
    out : user input --> int
    in : user input --> int 
    possibilité d'erreur si input != int
    '''
    while True:
        try:
            return int(input ("quelle table veux tu étudier ? ")) 
            break
        except ValueError:
            print ("peux tu faire attention ! entre un nombre entier")
    

def iteration():
    
    '''Faire une iteration 10 fois et augmenter un compteur pour un bonne reponse


        afficher la question avec table et random_number
        verifier si la réponse est bonne


    calculer le nombre de reponses correcter avec un counteur (count)
    out : table --> int
		  random_number --> int
    in: counter --> int
        

    robustesse : verifier que reponse est int
    '''

    counter = 0
    random_n_old = 0 # sert de control pour ne pas répter la même question. doit être ini avant for loop
    table = demander_table()
    temps_max = 0
    temps_total = 0
    
    for i in range (0, 10):
        #generer nombre aleatoire et verifier qu'il n'y a pas de
        #repetition
        random_number = random.randint(1, 11)
        #vérifier que la même operation n'eas pas proposé deux fois de suite
        while random_number == random_n_old:    
            random_number = random.randint(1, 11)
        random_n_old = random_number

        # mesurer le temps de reponse
        temps_avant = (time.monotonic())
        while True:
            try:
                reponse = int(input (f"{table} x {random_number} = "))
                break
            except ValueError:
                print (f"Une multiplication de deux nombres entiers est forcement un nombre entnier, essaie encore")
        temps_apres = (time.monotonic())
        assert type(reponse) == int
        # calculer le temps de reponse
        temps_parcuru = (temps_apres - temps_avant)
        temps_total += temps_parcuru
        # identifier la reponse la plus lente
        if temps_parcuru > temps_max:
            reponse_lente = (f"{table} * {random_number}") 
        # compter le nombre de bonnes réponses
        if reponse == table * random_number:
            print ("Correct")
            counter += 1
        else:
            print ("Erreur")
    assert type(counter) == int
    return counter, temps_total, reponse_lente
 

def afficher_resultat():

    '''Afficher le resultat du programme

        out : counter --> int

    '''
    counter, temps_total, reponse_lente = iteration()
    if counter == 10:
        print (f"{counter} reponses sur 10, Excellent !")
    elif counter == 9:
        print (f"{counter} reponses sur 10, Trés bien ")
    elif 7 <= counter <= 8:
        print (f"{counter} reponses sur 10, bien ")
    elif 4 <= counter <= 6:
        print (f"{counter} reponses sur 10, moyen ")
    elif 1 <= counter <= 3:
        print (f"{counter} reponses sur 10, Il faut retravailler cette table") 
    elif counter == 0:
        print (f"{counter} reponses sur 10, Est ce que tu l'as fait exprès ?")
	
    print (f"Le temps moyen de reponse est de {temps_total/10}")
    print (f"T'as mis le plus de temps à resoudre {reponse_lente}")




main()

